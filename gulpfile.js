var gulp = require("gulp"),
  injectPartials = require("gulp-inject-partials"),
  browserSync = require("browser-sync"),
  autoprefixer = require("gulp-autoprefixer"),
  concat = require("gulp-concat"),
  minify = require("gulp-minify"),
  sass = require("gulp-sass")(require("sass"));

gulp.task(
  "watch",
  gulp.series(function () {
    //СОЗДАНИЕ ВИРТУАЛЬНОГО СЕРВЕРА
    browserSync({
      server: {
        baseDir: "./build",
        index: "index.html",
        directory: true,
      },
      watchTask: true,
    });
    //ПОДКЛЮЧЕНИЕ ВНЕШНИХ СТИЛЕЙ

    //ПОДКЛЮЧЕНИЕ ВНЕШНИХ СКРИПТОВ
    gulp
      .src([
        "node_modules/slick-carousel/slick/slick.min.js", // Подключаем Slick
      ])
      .pipe(concat("libs.js"))
      .pipe(
        minify({
          ext: {
            min: ".min.js",
          },
          ignoreFiles: ["-min.js"],
        })
      )
      .pipe(gulp.dest("build/js"));

    //ОТСЛЕЖИВАНИЕ ИЗМЕНЕНИЙ

    //ИЗМЕНЕНИЕ СТИЛЕЙ
    var css = gulp.watch("./src/scss/**/*.sass");
    css.on("change", function () {
      gulp
        .src("src/scss/main.sass")
        .pipe(sass())
        .pipe(autoprefixer(["last 2 versions"], { cascade: true }))
        .pipe(gulp.dest("build/css"));

      browserSync.reload();
    });
    //ИЗМЕНЕНИЕ СТРУКТУРЫ
    var html = gulp.watch("./src/templates/**/*.html");
    html.on("change", function () {
      gulp
        .src("./src/templates/*.html")
        .pipe(injectPartials())
        .pipe(gulp.dest("./build"));

      browserSync.reload();
    });
    //ИЗМЕНЕНИЕ СКРИПТОВ
    var js = gulp.watch("./src/js/*.js");
    js.on("change", function () {
      gulp.src(["./src/js/main.js"]).pipe(gulp.dest("build/js"));
      browserSync.reload();
    });
  })
);
