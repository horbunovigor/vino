(function ($) {
  ("use strict");
  // variables
  var layout = $(".layout"),
    header = $(".layout__header");
  preloader();
  function preloader() {
    layout.on("click", ".nav__link", function (event) {
      let index = $(this).data("index");
      $(".layout__slider").slick("slickGoTo", index - 1);
    });
    setTimeout(function () {
      layout.addClass("layout_ready-load");
    }, 100);
  }

  $(window).on("load resize", function () {
    if ($(window).width() > 767) {
      // let slide = document.querySelector(".slick-current");
      if ($(".layout__slider").length) {
        $(".layout__slider").slick({
          slidesToShow: 1,
          slidesToScroll: 1,
          infinite: false,
          arrows: false,
          fade: true,
          speed: 1000,
          draggable: false,
          swipe: false,
          swipeToSlide: false,
          touchMove: false,
          draggable: false,
          accessibility: false,
          arrows: false,
        });

        $(".layout__slider").on("afterChange", function (currentSlide) {
          let index = $(".layout__slider").find(".slick-current").index() + 1;
          if (index == 2 || index == 3) {
            $(".layout__background").addClass("layout__background_disabled");
          } else {
            $(".layout__background").removeClass("layout__background_disabled");
          }
          visibleElements(index);
        });
      } else if ($(".layout__slider").hasClass("slick-initialized")) {
        $(".layout__slider").slick("unslick");
      }

      // Scrolll
      let slider = $(".layout__slider");
      // Carousel
      let scrollContainer = document.querySelector(".layout__carousel");
      scrollContainer.addEventListener("wheel", (evt) => {
        evt.preventDefault();
        if (
          (evt.deltaY > 0) &
          (Math.ceil(
            scrollContainer.scrollLeft + scrollContainer.clientWidth
          ) >=
            Math.ceil(scrollContainer.scrollWidth))
        ) {
          slider.slick("slickNext");
        } else if (
          evt.deltaY <= 0 &&
          Math.ceil(scrollContainer.scrollLeft) === 0
        ) {
          slider.slick("slickPrev");
        }
        $(".carousel__item").each(function () {
          if (scrollContainer.scrollLeft >= $(this).offset().left) {
            $(this).addClass("carousel__item_active");
          } else {
            $(this).removeClass("carousel__item_active");
          }
        });
        scrollContainer.scrollLeft += evt.deltaY;
      });

      // Carousel Services
      let scrollServices = document.querySelector(".services__list");
      scrollServices.addEventListener("wheel", (evt) => {
        evt.preventDefault();
        if (
          (evt.deltaY > 0) &
          (Math.ceil(scrollServices.scrollLeft + scrollServices.clientWidth) >=
            Math.ceil(scrollServices.scrollWidth))
        ) {
          slider.slick("slickNext");
        } else if (
          evt.deltaY <= 0 &&
          Math.ceil(scrollServices.scrollLeft) === 0
        ) {
          slider.slick("slickPrev");
        }
        scrollServices.scrollLeft += evt.deltaY;
      });

      // Scroll
      document.addEventListener("wheel", (evt) => {
        let slideCurrent = slider.find(".slick-current"),
          sliderIndex = slider.find(".slick-current").index() + 1;
        if (
          !slideCurrent.find(".layout__carousel").length &&
          !slideCurrent.find(".services__list").length
        ) {
          // Next
          if (
            (evt.deltaY > 0) &
            (Math.ceil(slideCurrent.scrollTop() + slideCurrent.innerHeight()) >=
              Math.ceil(slideCurrent.prop("scrollHeight")))
          ) {
            slider.slick("slickNext");
          }
          // Prev
          if ((evt.deltaY < 0) & Math.ceil(slideCurrent.scrollTop() === 0)) {
            slider.slick("slickPrev");
          }

          // Animation
          visibleElements(sliderIndex);

          if (Math.ceil(slideCurrent.scrollTop() > 100)) {
            slideCurrent
              .addClass("slider__slide_animation")
              .siblings()
              .removeClass("slider__slide_animation");
          }
        }
      });

      function visibleElements(sliderIndex) {
        let slideCurrent = slider.find(".slick-current");
        // Header
        if (slideCurrent.scrollTop() === 0 && sliderIndex == 1) {
          $(".layout__header").addClass("layout__header_animation");
        } else {
          $(".layout__header").removeClass("layout__header_animation");
        }
        // Footer
        if (sliderIndex === $(".slider__slide").length) {
          $(".layout__footer").addClass("layout__footer_animation");
        } else {
          $(".layout__footer").removeClass("layout__footer_animation");
        }
        // Widget
        if (sliderIndex !== $(".slider__slide").length) {
          $(".layout__widget").addClass("layout__widget_animation");
        } else {
          $(".layout__widget").removeClass("layout__widget_animation");
        }
      }
    }
  });

  // Menu
  navInit();
  function navInit() {
    header.find(".header__burger").on("click", function () {
      $(this).closest(header).toggleClass("layout__header_menu-active");
    });
  }
})(jQuery);
